# Php-fpm Docker images

## Php modules and version based on Alpine Linux

### Php 7.1.21

* [Codecasts Rocks Php 7.1.21](http://php.codecasts.rocks/v3.7/php-7.1/x86_64)
* [Alpine packages Php 7.1.17](https://pkgs.alpinelinux.org/packages?name=php7*&branch=v3.7&arch=x86_64)

### Php 7.2.12

* [Codecasts Rocks Php 7.2.12](http://php.codecasts.rocks/v3.8/php-7.2/x86_64)
* [Alpine packages Php 7.2.10](https://pkgs.alpinelinux.org/packages?name=php7*&branch=v3.8&arch=x86_64)

Script file _run.sh_ is common but need to be inside the Dockerfile folder for building.
